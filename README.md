Give a date string to get it in mm/dd/yy or mm/dd/yyyy format.
Example usage: 
python test.py 22 jan
python test.py 02/18/2017
python test.py 3rd dec

etc.