__author__ = 'ayushjain'
import argparse
import re

parser = argparse.ArgumentParser(description='Date parser script')
parser.add_argument('-date', metavar='N', type=str, nargs='+',
                    help='Give a date string to get it in mm/dd/yy or mm/dd/yyyy format.\n'
                         'Ex: python test.py 22 jan')

months = {'january': '1',
          'february': '2',
          'march': '3',
          'april': '4',
          'may': '5',
          'june': '6',
          'july': '7',
          'august': '8',
          'september': '9',
          'october': '10',
          'november': '11',
          'december': '12'}

users = [{"id": 1, "dob": "1991-mar-12"},
         {"id": 2, "dob": "03-91-12"},
         {"id": 3, "dob": "1991-mar-12"},
         {"id": 4, "dob": "19-03-91"},
         {"id": 5, "dob": "19-03-1991"},
         {"id": 6, "dob": "03-19-1991"},
         {"id": 7, "dob": "sep-91-12"},
         {"id": 8, "dob": "1991-03-sep"},
         {"id": 9, "dob": "1991-03-12"},
         {"id": 10, "dob": "91-oct-12"},
         {"id": 11, "dob": "Feb 2017"},
         {"id": 12, "dob": "2nd Oct"},
         {"id": 13, "dob": "10/10"},
         {"id": 14, "dob": "Jan 3rd"}]


class DateParser(object):
    def __init__(self, date_string):
        self.date_string = date_string

    @staticmethod
    def get_month(month):
        """
        Any number less then 12 can be considered as a Month
        Any string in the date i.e. 'jan', 'feb', 'mar' will be considered as a Month
        """
        result = None
        if month.isdigit():
            if int(month) <= 12:
                result = month
        else:
            result = month
        return result

    @staticmethod
    def get_year(year):
        """
        Any number greater then 32 will be a year.
        """
        result = None
        if year.isdigit():
            if int(year) > 31:
                result = year
        return result

    @classmethod
    def get_date(cls, date_string):
        """
        :param date_string: str (1991-mar-12)
        :returns datetime.datetime object
        """
        # Date list from date string.
        # This will delimit the string from every other char except from any number or alphabet
        date_list = re.findall(r"[\w']+", date_string)
        date_length = len(date_list)
        if date_length:
            # Getting year, if not found, consider last part as Year
            year_str = next((y for y in date_list if y.isdigit() and int(y) > 31),
                            date_list[2] if date_length > 2 else date_list[0])
            year = cls.get_year(year_str) or cls.get_year(date_list[0]) or cls.get_year(
                date_list[1]) if date_length > 1 else date_list[0] or date_list[2] if date_length > 2 else date_list[0]
            date_list.remove(year) if year else None

            # Getting month, if not found, consider last part as Month
            month_str = next((m for m in date_list if not bool(re.search(r'\d', m))), date_list[0])
            month = cls.get_month(month_str) or cls.get_month(date_list[1]) if date_length > 1 else date_list[
                                                                                                        0] or cls.get_month(
                date_list[2]) if date_length > 2 else date_list[0] or date_list[0]
            date_list.remove(month) if month else None
            month = next(v for k, v in months.items() if month.lower() in k) if not month.isdigit() else month
            month = ''.join(['0', month]) if len(month) == 1 else month

            # After Year and Month removing from date, only last part is Date
            regex = re.compile('[^0-9]')
            date = regex.sub('', date_list[0]) if date_list else None
            date = ''.join(['0', date]) if date and len(date) == 1 else date

            result = '/'.join([month, date or '-', year or '-'])
        else:
            result = "Please provide a valid date"

        return result


args = parser.parse_args()
date = args.date
if date:
    print('date (mm/dd/yy or yyyy): ', DateParser.get_date(' '.join(date)))
else:
    for user in users:
        dob = user.get('dob')
        if dob:
            user['dob (mm/dd/yy or yyyy)'] = DateParser.get_date(dob)
            print(user)
